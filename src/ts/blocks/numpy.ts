import * as Blockly from "blockly";

export function setNumpy() {
  // Déclarations
  var numpy_array = {
    message0: Blockly.Msg["numpy_array"],
    args0: [
      {
        type: "input_value",
        name: "array_value",
        check: null,
      },
    ],
  };
  Blockly.Blocks["numpy_array"] = {
    init: function () {
        this.jsonInit(numpy_array);
      this.setOutput(true, "String");
      this.setColour("%{BKY_MATH_COLOR}");
      this.setTooltip("Crée un tableau de valeurs");
      this.setHelpUrl("https://numpy.org/doc/stable/reference/generated/numpy.array.html");
      this.updateShadow();
    },
    updateShadow: function () {
        var connection = this.getInput("array_value").connection;
        var otherConnection = connection.targetConnection;
        var dom = Blockly.Xml.textToDom(
          "<xml>" +
            '  <shadow type="list_def"><field name="VALUE">1,2,3,4</field></shadow>' +
            "</xml>"
        ).children[0];
        connection.setShadowDom(dom);
        connection.respawnShadow_();
        connection.connect(otherConnection);
      },
  };

  var numpy_polyfit = {
    message0: Blockly.Msg["numpy_polyfit"],
    args0: [
      {
        type: "field_variable",
        name: "x",
        variable: "x"
      },
      {
        type: "field_variable",
        name: "y",
        variable: "y"
      },
      {
        type: "field_number",
        name: "deg",
        value: 1,        
      },
    ],
  };
  Blockly.Blocks["numpy_polyfit"] = {
    init: function () {
        this.jsonInit(numpy_polyfit);
      this.setOutput(true, "String");
      this.setColour("%{BKY_MATH_COLOR}");
      this.setTooltip("Crée un modèle de régression polynomiale");
      this.setHelpUrl("https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html#numpy-polyfit");
    },
  };

}
